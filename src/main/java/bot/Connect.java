package bot;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.Config;
import utils.MyListener;
import utils.TimeyWimey;

import javax.security.auth.login.LoginException;

public class Connect {

    //Log variable
    private static final Logger log = LoggerFactory.getLogger(Connect.class);

    //Prefix for commands
    public static final String PREFIX = "~!";

    //JDA instance
    public static JDA discord = null;

    //Database Key
    public static final String DATABASE = "C:\\databases\\SquishyData.db";

    //Connects the bot to discord
    public static void main(String[] args) throws LoginException, InterruptedException, RateLimitedException {
        discord = new JDABuilder(AccountType.BOT)
                .setGame(Game.playing("Tech Team | ~!help"))
                .setToken(Config.DISCORD_TOKEN)
                .addEventListener(new MyListener())
                .buildBlocking();

        //Runs the schedule code
        TimeyWimey.updateCalender();
    }
}