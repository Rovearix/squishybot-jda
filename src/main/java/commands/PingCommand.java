package commands;

import net.dv8tion.jda.core.entities.MessageChannel;

public class PingCommand {

    //Test command
    public static void run(MessageChannel channel) {

        //Sends a message and edits it
        channel.sendMessage("Pong!").complete().editMessage("Pranked").complete();
    }
}
