package commands;

import bot.Connect;
import data.DateData;
import net.dv8tion.jda.core.entities.MessageChannel;
import utils.Config;
import utils.DatabaseManager;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class PostSchedule {

    //Automatic posting done by the TimeyWimey class
    public static void autoPost() {

        //Selects the Tech team #fun channel to post the schedule in
        MessageChannel channel = Connect.discord.getTextChannelById(Config.POST_CHANNEL);

        //Sends the message
        TimesCommand.run(channel);
    }

    //Automated post if school is canceled
    public static void canceled() {

        //Selects the Tech team #fun channel to post the schedule in
        MessageChannel channel = Connect.discord.getTextChannelById(Config.POST_CHANNEL);

        //Sends the cancellation message
        channel.sendMessage("```Hey guys ~ school is canceled today due to unforeseen circumstances. \n" +
                "Have fun on your day off!```").queue();
    }

    //Posts the schedule from a user command
    public static void post(MessageChannel channel) {

        //Checks for school cancellation
        if (DateData.check(getDate()).equals("No School") || DateData.check(getDate()).equals("Cancel School")) {

            //Check for Tomorrow
            int hour = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(11, 13));
            int minute = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(14, 16));
            String now = Calendar.getInstance().getTime().toString();
            if ((hour >= 15 || (hour == 14 && minute > 32)) || now.charAt(0) == 'S')
                channel.sendMessage("There is no school tomorrow.").queue();
            else
                channel.sendMessage("There is no school today.").queue();
        } else
            channel.sendMessage(genSchedule()).queue();
    }

    //Generates the format with the schedule data
    private static String genSchedule() {

        //Variable to hold the schedule
        String schedule = "";

        //Prints out the next day's schedule after school ends
        int hour = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(11, 13));
        int minute = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(14, 16));
        String now = Calendar.getInstance().getTime().toString();
        if ((hour >= 15 || (hour == 14 && minute > 32)) || now.charAt(0) == 'S') {
            schedule += "Tomorrow's schedule is as following:\n";
            if (now.charAt(0) == 'F' || now.charAt(1) == 'a')
                return "There is no school tomorrow.";
        }

        //Adds the correct information
        schedule += "```";
        schedule += "Day:   " + getLetterDay() + "                          Date: " + getDate() + "\n";
        schedule += "Order: " + getOrder() + "                Type: " + DateData.check(getDate());
        schedule += "```";

        //Returns generated schedule
        return schedule;
    }

    //Gets the letter day from a file
    public static char getLetterDay() {

        //Checks through each line
        char letter = DatabaseManager.getResults("Today", "Letter").get(0).charAt(0);

        //Prints out the next day's schedule after school ends
        int hour = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(11, 13));
        int minute = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(14, 16));
        String now = Calendar.getInstance().getTime().toString();
        if ((hour >= 15 || (hour == 14 && minute > 32) || (hour < 7)) || now.charAt(0) == 'S') {
            letter++;
            if (letter == 'I')
                letter = 'A';
        }
        return letter;
    }

    //Gets the current date in a correct format
    public static String getDate() {

        //New object of calender
        Calendar calendar = Calendar.getInstance();

        //Checks for next day
        int hour = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(11, 13));
        int minute = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(14, 16));
        String now = Calendar.getInstance().getTime().toString();
        if ((hour >= 15 || (hour == 14 && minute > 32) || now.charAt(0) == 'S'))
            calendar.add(Calendar.DAY_OF_YEAR, 1);

        //Converts Date to LocalDate for formatting purposes
        LocalDate localDate = calendar.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        //Returns the date in a simple format
        return DateTimeFormatter.ofPattern("MM/dd/yyyy").format(localDate);
    }

    //Gets the order of the current school day
    static String getOrder() {
        char letter = getLetterDay();
        if (letter == 'A')
            return "1 2 3 4 5 6";
        else if (letter == 'B')
            return "7 8 1 2 3 4";
        else if (letter == 'C')
            return "5 6 7 8 1 2";
        else if (letter == 'D')
            return "3 4 5 6 7 8";
        else if (letter == 'E')
            return "6 5 4 3 2 1";
        else if (letter == 'F')
            return "4 3 2 1 8 7";
        else if (letter == 'G')
            return "2 1 8 7 6 5";
        else
            return "8 7 6 5 4 3";
    }
}