package commands;

import bot.Connect;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import utils.DatabaseManager;

public class UpdateData {

    //Boolean to hold weather or not the command is active
    public static boolean activated = false;

    //Object to hold the current user
    public static User executor = Connect.discord.getSelfUser();

    //Integer to remember the current step
    private static int step = 1;

    //String to hold the date
    private static String date = "";

    //Method that parses the initial command
    public static void parse(MessageReceivedEvent event) {

        //Checks for command override
        if (activated)
            busy(event);
        else
            startDialog(event);
    }

    //Sends out the starting information
    private static void startDialog(MessageReceivedEvent event) {

        //Gets the current message channel
        MessageChannel channel = event.getChannel();

        //Generates the initial info
        String dialog = event.getAuthor().getAsMention() + ", welcome to the update dialog!\n";
        dialog += "To cancel at any time, type `quit`\n";
        dialog += "Type the date of the override `mm/dd/yyyy`";

        //Sends the message with instructions
        channel.sendMessage(dialog).queue();

        //Switches on a listener
        activated = true;

        //Saves current user
        executor = event.getAuthor();
    }

    //Handles the multi-line update
    public static void updateDialog(MessageReceivedEvent event) {

        //Gets the message
        Message message = event.getMessage();
        String content = message.getContentRaw();

        //Gets the current channel
        MessageChannel channel = event.getChannel();

        //Cleans up the message for the commands to process
        content = content.toUpperCase().replace(" ", "");

        //Checks for user cancellation
        if (content.equalsIgnoreCase("quit")) {
            resetData();
            channel.sendMessage("Canceling Request!").queue();
            return;
        }

        //Runs dialog according to the current step
        if (step == 1) {

            //Checks for the exact format
            if (content.length() == 10 && content.indexOf("/") == 2 && content.lastIndexOf("/") == 5) {

                //Adds the date and identifier to the changes
                date = content;

                //Increments the current step
                step++;

                //Sends the next-step message
                channel.sendMessage(executor.getAsMention() + " Enter the number of the day type change, the valid options are: \n" +
                        "1) `Cancel School` \n" +
                        "2) `Two Hour Delay` \n" +
                        "3) `Three Hour Delay` \n" +
                        "4) `Early Dismissal` \n" +
                        "5) `Emergency Early Dismissal` \n" +
                        "6) `PLC` \n" +
                        "7) `Regular` \n" +
                        "8) `None`").queue();
            } else
                channel.sendMessage("Invalid date entered. Format: `mm/dd/yyyy`, \nIf the date has a single digit, the first zero is required").queue();
        } else if (step == 2) {

            //Checks if choice is valid
            if (content.equals("1") || content.equals("2") || content.equals("3") || content.equals("4") || content.equals("5") || content.equals("6") || content.equals("7") || content.equals("8")) {

                //Switch statement for choice
                switch (content) {
                    case "1":
                        DatabaseManager.addEvent(date, "Cancel School");
                        break;
                    case "2":
                        DatabaseManager.addEvent(date, "Two Hour Delay");
                        break;
                    case "3":
                        DatabaseManager.addEvent(date, "Three Hour Delay");
                        break;
                    case "4":
                        DatabaseManager.addEvent(date, "Early Dismissal");
                        break;
                    case "5":
                        DatabaseManager.addEvent(date, "Emergency Early Dismissal");
                        break;
                    case "6":
                        DatabaseManager.addEvent(date, "PLC");
                        break;
                    case "7":
                        DatabaseManager.addEvent(date, "Regular");
                        break;
                    default:
                        channel.sendMessage("Canceling request!").queue();
                        resetData();
                        return;
                }

                //Sends the completed message
                channel.sendMessage("Finished writing changes!").queue();

                //Resets the temp data
                resetData();

            } else
                channel.sendMessage("Invalid choice, please try again!").queue();
        }
    }

    //Resets temp data
    private static void resetData() {
        activated = false;
        executor = Connect.discord.getSelfUser();
        date = "";
        step = 1;
    }

    //Busy message if multiple people try to use the command
    private static void busy(MessageReceivedEvent event) {

        //Variable to hold the origin channel
        MessageChannel channel = event.getChannel();

        //Sends the busy message
        channel.sendMessage("Sorry, another user is using this command. Check back later!").queue();
    }
}