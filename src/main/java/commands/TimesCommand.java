package commands;

import data.DateData;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.util.Calendar;

import static commands.PostSchedule.getDate;
import static data.DateData.*;

public class TimesCommand {
    
    //Runs the command
    public static void run(MessageChannel channel) {

        if (DateData.check(getDate()).equals("No School") || DateData.check(getDate()).equals("Cancel School")) {
            channel.sendMessage("There is no school for today.").queue();
            return;
        }

        //StringBuilder to hold times info
        StringBuilder times = new StringBuilder("```");
    
        //Checks for which schedule to post
        int hour = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(11, 13));
        int minute = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(14, 16));
        if (hour >= 15 || (hour == 14 && minute > 32))
            times.append("Tomorrow");
        else
            times.append("Today");
        
        //Adds the date information
        times.append(String.format(" is a %c day and will be following the %s schedule:", PostSchedule.getLetterDay(), DateData.check(getDate())));

        //Gets today's current times
        String[] todaysTimes = getTimes();

        //Checks for Advisory or PLC
        if (DateData.check(getDate()).equals("Advisory") || DateData.check(getDate()).equals("PLC"))
            times.append(String.format("\n%s: %s", DateData.check(getDate()), todaysTimes[9]));


        //Adds the first 5 classes to the message
        for (int i = 0; i < 6; i++) {
            
            //Adds the current class start and end time to the message
            times.append(String.format("\nClass %s: %s", PostSchedule.getOrder().replace(" ", "").charAt(i), todaysTimes[i]));
            
            //Checks for lunch occurrence
            if (i == 4 && todaysTimes.length > 7)
                for (int w = 1; w <= 3; w++)
                    times.append(String.format("\n Wave %d: %s", w, todaysTimes[5 + w]));
        }
        
        //Adds the code block ending identifier
        times.append("```");
        
        //Adds the period times
        channel.sendMessage(times).queue();
    }
    
    //Method to find which times we are using today
    private static String[] getTimes() {
        
        //Switch that checks type of day against lists
        switch (DateData.check(getDate())) {
            case "Advisory":
                return ADVISORY_TIMES;
            case "PLC":
                return PLC_TIMES;
            case "Early Dismissal":
                return EARLY_DISMISSAL_TIMES;
            case "Two Hour Delay":
                return TWO_HOUR_DELAY_TIMES;
            case "Emergency Early Dismissal":
                return EMERGENCY_EARLY_DISMISSAL_TIMES;
            case "Three Hour Delay":
                return THREE_HOUR_DELAY_TIMES;
            default:
                return REGULAR_TIMES;
        }
    }
}