package commands;

import bot.Connect;
import net.dv8tion.jda.core.entities.MessageChannel;
import utils.Config;

public class AnnounceCommand {

    //Sends a message from an executive in the voice of Mr.Squishy
    public static void run(String message) {

        //Selects the Tech team #fun channel to post the schedule in
        MessageChannel channel = Connect.discord.getTextChannelById(Config.POST_CHANNEL);

        //Sends the message
        channel.sendMessage(message.replace("announce ", "")).queue();
    }
}
