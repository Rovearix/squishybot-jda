package commands;

import bot.Connect;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class HelpCommand {

    //Parses data from the message
    public static void parse(MessageReceivedEvent event) {

        //Gets the message for parsing
        Message message = event.getMessage();
        String content = message.getContentRaw();

        //Cleans the message
        content = content.replace("~!help", "");
        content = content.replaceAll(" ", "");

        //Variable to store which help message sent
        String helpMessage = event.getAuthor().getAsMention() + ", ";

        //Variable that holds corresponding channel
        MessageChannel channel = event.getChannel();

        //Finds the correct help message
        if (content.equalsIgnoreCase("ping"))
            helpMessage += ping();
        else if (content.equalsIgnoreCase("order"))
            helpMessage += order();
        else if (content.equalsIgnoreCase("update"))
            helpMessage += update();
        else if (content.equalsIgnoreCase("help"))
            helpMessage += help();
        else if (content.equalsIgnoreCase("times"))
            helpMessage += times();
        else if (content.equalsIgnoreCase("announce"))
            helpMessage += announce();
        else if (content.length() == 0)
            helpMessage += blank();
        else
            helpMessage = notFound();

        //Sends the help message to the channel
        channel.sendMessage(helpMessage).queue();
    }

    //Returns a message about the ping command
    private static String ping() {
        return "The `ping` command is for testing the the bot's command system";
    }

    //Returns a message about the order command
    private static String order() {
        return "The `order` command posts the schedule early.";
    }

    //Returns a message about the update command
    private static String update() {
        return "The `update` command is used to add unplanned events to the database.";
    }

    //Returns a message about the update command
    private static String announce() {
        return "The `announce` command is used by executives to send a message to the server.";
    }

    //Returns a message about the help command
    private static String help() {
        return "This command is too hard to explain how to use; try watching a few episodes of Rick and Morty.";
    }

    //Returns a message about the times command
    private static String times() {
        return "The `times` command posts more in-depth information about today's schedule";
    }

    //Returns a message if no command specified
    private static String blank() {
        String message = "The whole purpose of me is to send our schools schedule every day at 7 AM EST";
        message += "\nFor information on a particular command, do `~!help` + `commandname`";
        message += "\n\nI have the following commands: `help`, `ping`, `order`, `times`,  `update`, and `announce`";
        return message;
    }

    //Message if a command is not found
    public static String notFound() {
        return "Sorry, that command was not found! Do " + Connect.PREFIX + "help for information.";
    }
}