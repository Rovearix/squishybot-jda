package data;

import utils.DatabaseManager;

import java.util.Calendar;

/**
 * This class contains data from the year 2018
 */
public class DateData {

    //Checks the current data against the list of planned anomalies
    public static String check(String today) {

        //On mondays, we use the advisory schedule
        if (DatabaseManager.getDateData(today).equals("Regular") && isAdvisory())
            return "Advisory";

        //The day is regular if not listed
        return DatabaseManager.getDateData(today);
    }

    private static boolean isAdvisory() {

        //New object of calender
        Calendar calendar = Calendar.getInstance();

        //Variable to hold current day
        String day = calendar.getTime().toString().substring(0, 3);

        //Records time data
        int hour = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(11, 13));
        int minute = Integer.parseInt(Calendar.getInstance().getTime().toString().substring(14, 16));

        //Checks for Tomorrow
        if (day.equals("Mon") && (hour >= 15 || (hour == 14 && minute > 32))) {
            if (day.equals("Fri"))
                day = "Mon";
            else
                return false;
        }

        //Checks for Weekend
        if (day.equals("Sat") || day.equals("Sun"))
            day = "Mon";

        //Checks for Advisory Occurrence
        return day.equals("Mon");
    }

    //Times for the regular school days
    public static final String[] REGULAR_TIMES = {
            "8:00 AM - 8:56 AM", "9:01 AM - 9:55 AM", "10:00 AM - 10:54 AM", "10:59 AM - 11:53 AM",
            "11:57 AM - 1:35 PM", "1:39 PM - 2:32 PM",
            "11:57 AM - 12:27 PM", "12:31 PM - 1:01 PM", "1:05 PM - 1:35 PM"
    };

    //Times for the PLC days
    public static final String[] PLC_TIMES = {
            "8:51 AM - 9:34 AM", "9:39 AM - 10:22 AM", "10:27 AM - 11:10 AM", "11:15 AM - 11:58 AM",
            "12:03 PM - 1:43 PM", "1:48 PM - 2:32 PM",
            "12:03 PM - 12:33 PM", "12:38 PM - 1:08 PM", "1:13 PM - 1:43 PM", "8:00 AM - 8:46 AM"
    };

    //Times for the Advisory days
    public static final String[] ADVISORY_TIMES = {
            "8:25 AM - 9:14 AM", "9:19 AM - 10:08 AM", "10:13 AM - 11:02 AM", "11:07 AM - 11:56 AM",
            "12:01 PM - 1:39 PM", "1:43 PM - 2:32 PM",
            "12:01 PM - 12:31 PM", "12:35 PM - 1:05 PM", "1:09 PM - 1:39 PM", "8:00 AM - 8:20 AM"
    };

    //Times for the Early Dismissal days
    public static final String[] EARLY_DISMISSAL_TIMES = {
            "8:00 AM - 8:42 AM", "8:47 AM - 9:28 AM", "9:33 AM - 10:14 AM", "10:19 AM - 11:00 AM",
            "11:05 AM - 11:46 AM", "11:51 AM - 12:32 PM",
    };

    //Times for the Emergency Early Dismissal days
    public static final String[] EMERGENCY_EARLY_DISMISSAL_TIMES = {
            "8:00 AM - 8:32 AM", "8:37 AM - 9:08 AM", "9:13 AM - 9:44 AM", "9:49 AM - 10:20 AM",
            "10:25 AM - 10:56 AM", "11:01 AM - 11:32 AM",
    };

    //Times for the Two Hour Delay days
    public static final String[] TWO_HOUR_DELAY_TIMES = {
            "10:00 AM - 10:31 AM", "10:36 AM - 11:07 AM", "11:12 AM - 11:42 AM", "11:47 AM - 12:17 PM",
            "12:21 PM - 1:59 PM", "2:03 PM - 2:32 PM",
            "12:21 PM - 12:51 PM", "12:55 PM - 1:25 PM", "1:29 PM - 1:59 PM"
    };

    //Times for the Two Hour Delay days
    public static final String[] THREE_HOUR_DELAY_TIMES = {
            "00:00 AM - 00:00 AM", "00:00 AM - 00:00 AM", "00:00 AM - 00:00 AM", "00:00 AM - 00:00 PM",
            "00:00 PM - 00:00 PM", "00:03 PM - 00:00 PM",
            "00:00 PM - 00:00 PM", "00:00 PM - 0:00 PM", "00:00 PM - 00:00 PM"
    };
}