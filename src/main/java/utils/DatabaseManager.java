package utils;

import bot.Connect;

import java.sql.*;
import java.util.ArrayList;

public class DatabaseManager {

    //Returns results based on passed table and field
    public static ArrayList<String> getResults(String table, String field) {
        ArrayList<String> results = new ArrayList<>();
        try {
            Connection dbConn = DriverManager.getConnection("jdbc:sqlite:" + Connect.DATABASE);
            Statement statement = dbConn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT " + field + " FROM " + table);
            while (resultSet.next())
                results.add(resultSet.getString(1));
            statement.close();
            resultSet.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }

    //Returns what the type of day it is on a given date
    public static String getDateData(String date) {
        String result = "Regular";
        try {
            Connection dbConn = DriverManager.getConnection("jdbc:sqlite:" + Connect.DATABASE);
            Statement statement = dbConn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT Event FROM SchoolEvents WHERE Date IS \"" + date + "\"");
            while (resultSet.next())
                result = resultSet.getString(1);
            statement.close();
            resultSet.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //Sets the letter day in the database
    public static void setDay(String day) {
        try {
            Connection dbConn = DriverManager.getConnection("jdbc:sqlite:" + Connect.DATABASE);
            PreparedStatement statement = dbConn
                    .prepareStatement("UPDATE Today SET Letter= \"" + day + "\"");
            statement.executeUpdate();
            statement.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Adds a new event to the database
    public static void addEvent(String date, String event) {
        try {
            Connection dbConn = DriverManager.getConnection("jdbc:sqlite:" + Connect.DATABASE);
            PreparedStatement statement = dbConn
                    .prepareStatement("INSERT INTO SchoolEvents (Date, Event) " +
                            "VALUES(\"" + date + "\", \"" + event + "\")");
            statement.executeUpdate();
            statement.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
