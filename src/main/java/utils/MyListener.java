package utils;

import bot.Connect;
import commands.*;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class MyListener extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        //Disregards potential bots' commands
        if (event.getAuthor().isBot())
            return;

        //Checks for commands that need the listener
        if (UpdateData.activated && UpdateData.executor.equals(event.getAuthor()))
            UpdateData.updateDialog(event);

        //Gets the channel that requested the command
        MessageChannel channel = event.getChannel();

        //Gets the message
        Message message = event.getMessage();
        String content = message.getContentRaw();

        //Cleans prefix
        if (content.indexOf(Connect.PREFIX) == 0)
            content = content.replaceFirst(Connect.PREFIX, "");
        else
            return;

        //Runs Help command with data.
        if (content.length() >= 4 && content.substring(0, 4).equalsIgnoreCase("help")) {
            HelpCommand.parse(event);
            return;
        }

        //Runs ping command
        else if (content.equalsIgnoreCase("ping")) {
            PingCommand.run(channel);
            return;
        }

        //Runs post schedule command
        else if (content.equalsIgnoreCase("order")) {
            PostSchedule.post(channel);
            return;
        }

        //Runs times command
        else if (content.equalsIgnoreCase("times")) {
            TimesCommand.run(channel);
            return;
        }

        //Runs announce command
        else if (content.length() > 8 && content.substring(0, 8).equalsIgnoreCase("announce")) {
            if (Auth.checkUUID(event))
                AnnounceCommand.run(content);
            return;
        }

        //Runs update schedule command
        else if (content.length() >= 6 && content.substring(0, 6).equalsIgnoreCase("update")) {
            if (Auth.checkUUID(event))
                UpdateData.parse(event);
            return;
        }

        //Sends a message if the command is not found
        channel.sendMessage(HelpCommand.notFound()).queue();
    }

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
        System.out.println("[DM Event] " + event.getAuthor().getName() + " > " + event.getMessage().getContentRaw());
    }

}