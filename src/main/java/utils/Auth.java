package utils;

import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

class Auth {

    //Checks UUID of sender against Exec list
    static boolean checkUUID(MessageReceivedEvent event) {

        //Loops through all authorized UUIDs
        for (String uuid : Config.EXECUTIVE_UUIDS)
            if (event.getAuthor().getId().equals(uuid))
                return true;

        //Sends a reject message if not-authorized
        reject(event);

        //Returns false if not found
        return false;
    }

    //Message in the case of no auth
    private static void reject(MessageReceivedEvent event) {

        //Finds the message channel the command was sent in
        MessageChannel channel = event.getChannel();

        //Sends the reject message
        channel.sendMessage("Sorry, it looks like your account is not authorized!").queue();
    }
}
