package utils;

import java.util.ArrayList;

import static utils.DatabaseManager.getResults;

public class Config {

    //String to hold the bot's token
    public static final String DISCORD_TOKEN;

    //Channel ID to auto post the schedule
    public static final String POST_CHANNEL;

    //User IDs for executives of the Tech Team
    static final ArrayList<String> EXECUTIVE_UUIDS;

    //Gets the data when class is first noticed
    static {
        DISCORD_TOKEN = getResults("Config", "Token").get(0);
        POST_CHANNEL = getResults("Config", "Channel").get(0);
        EXECUTIVE_UUIDS = getResults("Access", "UUID");
    }
}