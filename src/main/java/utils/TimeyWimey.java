package utils;

import commands.PostSchedule;
import data.DateData;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static commands.PostSchedule.getDate;

public class TimeyWimey {

    //Calculates time to run
    public static void updateCalender() {

        //Calender variable
        Calendar calendar = Calendar.getInstance();

        //Gets the current time
        String time = calendar.getTime().toString();

        //Strips extraneous information
        for (int i = 1; i <= 3; i++)
            time = time.substring(time.indexOf(" ") + 1);

        //Variables with the current time data
        int hour = Integer.parseInt(time.substring(0, 2));
        int min = Integer.parseInt(time.substring(3, 5));
        int second = Integer.parseInt(time.substring(6, 8));

        //Calculates time between now and 7 AM
        int interval = (7 - hour) * 60 * 60 * 1000;
        if (hour > 7 || (hour == 7 && min + second > 0))
            interval += 24 * 60 * 60 * 1000;
        interval += (0 - min) * 60 * 1000;
        interval += (1 - second) * 1000;

        //Sends the data to the method
        schedule(new Date(System.currentTimeMillis() + interval));
    }

    //Schedules the post message event
    private static void schedule(Date timeToRun) {

        //New timer object
        Timer timer = new Timer();

        //Schedules the event for the passed date
        timer.schedule(new TimerTask() {

            //Method that runs when the time occurs
            public void run() {

                //Checks for planned no School
                if (!DateData.check(getDate()).equals("No School") && Calendar.getInstance().getTime().toString().charAt(0) != 'S') {

                    //Increments the day
                    incrementDay();

                    //Checks if school has been canceled and posts corresponding announcement
                    if (DateData.check(getDate()).equals("Cancel School"))
                        PostSchedule.canceled();
                    else
                        PostSchedule.autoPost();
                }

                //Re-schedules the event
                schedule(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
            }
        }, timeToRun);
    }

    //Increments the day by one
    private static void incrementDay() {

        //Variable to hold the current last day
        char today = PostSchedule.getLetterDay();

        //Checks for end of cycle week
        if (today == 'H')
            today = 'A';
        else
            today += 1;

        DatabaseManager.setDay("" + today);
    }
}
