# squishybot-jda [![Build Status](https://travis-ci.org/Ryert/squishybot-jda.svg?branch=master)](https://travis-ci.org/Ryert/squishybot-jda) [![CodeFactor](https://www.codefactor.io/repository/github/ryert/squishybot-jda/badge)](https://www.codefactor.io/repository/github/ryert/squishybot-jda)
A discord bot developed by Ryert with [JDA](https://github.com/DV8FromTheWorld/JDA)

# What does it do?
This bot sends a discord message with today's schedule for Newtown High School
